import React from "react";

class Goal extends React.Component {
    constructor(props) {
        super(props);
        this.isChecked = props.isChecked;
        this.goalText = props.goalText;
        this.doNothingOnChange = this.doNothingOnChange.bind(this);

    }

    doNothingOnChange() {

    }

    render() {
        return (
            <div>
                <div style={{
                    width: '95%',
                    backgroundColor: "#fff",
                    display: "block",
                    margin:"4px",
                    color: "#172b4d",
                    fontSize: "14px",
                    fontFamily: "-apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Noto Sans\", Ubuntu, \"Droid Sans\", \"Helvetica Neue\", sans-serif"
                }}>
                    <div>
                        <label>
                            <input
                                type="checkbox"
                                checked={this.props.isChecked}
                                onChange={this.doNothingOnChange}/>
                            {"  " + this.goalText}
                        </label>
                    </div>

                </div>

            </div>
        )
    }
}

export default Goal