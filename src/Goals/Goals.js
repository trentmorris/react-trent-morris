import React from "react";
import Goal from "./Goal";
import GoalHeader from "./GoalHeader";
import {Col, Row} from "react-bootstrap";

export default function Goals() {

    return (
        <div>
            <Row>
                <div style={{margin:"1%"}}></div>
                <div>
                    <div style={{width: "200px", backgroundColor: "#ebecf0", margin: "4px"}}>
                        <GoalHeader headerText="Squat Goals"></GoalHeader>
                        <Goal goalText="Squat 405" isChecked={true}/>
                        <Goal goalText="Squat 455" isChecked={false}/>
                        <Goal goalText="Squat 495" isChecked={false}/>
                        <br/>
                    </div>
                </div>
                <div>
                    <div style={{width: "200px", backgroundColor: "#ebecf0", margin: "4px"}}>
                        <GoalHeader headerText="Bench Goals"></GoalHeader>
                        <Goal goalText="Bench 300" isChecked={true}/>
                        <Goal goalText="Bench 315" isChecked={true}/>
                        <Goal goalText="Bench 350" isChecked={true}/>
                        <Goal goalText="Bench 365" isChecked={true}/>
                        <Goal goalText="Bench 385" isChecked={false}/>
                        <br/>
                    </div>
                </div>
                <div>
                    <div style={{width: "200px", backgroundColor: "#ebecf0", margin: "4px"}}>
                        <GoalHeader headerText="Deadlift Goals"></GoalHeader>
                        <Goal goalText="Deadlift 495" isChecked={true}/>
                        <Goal goalText="Deadlift 525" isChecked={true}/>
                        <Goal goalText="Deadlift 550" isChecked={true}/>
                        <Goal goalText="Deadlift 575" isChecked={false}/>
                        <br/>
                    </div>
                </div>

                <div>
                    <div style={{width: "200px", backgroundColor: "#ebecf0", margin: "4px"}}>
                        <GoalHeader headerText="Wakeboarding Goals"></GoalHeader>
                        <Goal goalText="Toeside Wake to Wake" isChecked={true}/>
                        <Goal goalText="Switch Wake to Wake" isChecked={true}/>
                        <Goal goalText="Heelside wake to wake 180" isChecked={true}/>
                        <Goal goalText="Backroll" isChecked={true}/>
                        <Goal goalText="Consistently landing Backroll each session" isChecked={false}/>
                        <Goal goalText="Scarecrow" isChecked={false}/>
                        <br/>
                    </div>
                </div>
            </Row>
        </div>

    );
}



