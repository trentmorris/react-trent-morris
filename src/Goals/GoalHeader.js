import React from "react";
import Goal from "./Goal";


class GoalHeader extends React.Component {
    constructor(props) {
        super(props);
        this.headerText = props.headerText;

    }
    render() {
        return (
            <h4 style={{
                color: "#172b4d",
                fontSize: "14px",
                fontWeight: 400,
                fontFamily: "-apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Noto Sans\", Ubuntu, \"Droid Sans\", \"Helvetica Neue\", sans-serif"
            }}>
                <b>
                &nbsp;{this.headerText}</b>
            </h4>
        )
    }
}

export default GoalHeader