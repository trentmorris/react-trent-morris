import React from 'react';
import logo from './logo.svg';
import './Admin/App.css';
import {NavLink} from "react-router-dom";
import {Row, Col} from "react-bootstrap"

function AppHeader() {
    return (
        <div>
            <div className="AppHeader">
                <header className="App-header">
                    <Row>
                        <img src={logo} className="App-logo" alt="logo"/>
                    </Row>
                </header>

            </div>
            <div className="links">
                <nav style={{marginLeft: "50px"}}>
                    <NavLink className="active" to="/">Home </NavLink>
                    <NavLink className="active" to="/TicTacToe">Tic Tac Toe </NavLink>
                    <NavLink className="active" to="/ConnectFour">Connect Four </NavLink>
                    <NavLink className="active" to="/Contact">Contact </NavLink>
                    <NavLink className="active" to="/Goals">Goals </NavLink>
                </nav>
            </div>
        </div>
    );
}

export default AppHeader;
