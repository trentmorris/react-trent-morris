import React from "react";
import "./c4square.css"

class C4square extends React.Component {
    constructor(props) {
        super(props);
        this.squareId = "square" + props.squareId
        this.handleClick= this.handleClick.bind(this);
    }

    handleClick(){
        this.props.handleClick(this.props.squareId);
    }

    squareColor() {
        if (this.props.squareValue === "R"){
            return "red"
        }
        else if (this.props.squareValue === "B"){
            return "black"
        }
        return "white"
    }

    render() {
        return (
            <button id={this.squareId} className="c4Square" onClick={this.handleClick}>
                <div className="circle" style={{background: this.squareColor()}}>
                </div>
            </button>
        )
    }
}

export default C4square