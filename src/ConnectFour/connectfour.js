import React from "react";
import C4square from "./c4square";
import {Col, Row} from "react-bootstrap";

class ConnectFour extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            gameBoard: ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-"],
            redTurn: true,
            message: "Red's turn to make a move.",
        }
        this.updateSquare = this.updateSquare.bind(this);
        this.resetBoardState = this.resetBoardState.bind(this);
    }

    render() {
        let elements = []
        for (let i = 0; i < 42; i++) {
            if (i % 7 === 0) {
                elements.push(<br key={"br"+i}/>)
            }
            elements.push(<C4square key={"square" + i} squareId={i} squareValue={this.state.gameBoard[i]} handleClick={this.updateSquare}/>)

        }

        return (
            <div>
                <Row>
                    <Col md={{offset:4}}>
                    </Col>
                </Row>
                <Row>
                    <Col md={{span:1, offset:1}}>
                        <br/>
                        <button id="resetButton" className="row-cols-md-3" onClick={this.resetBoardState}>Reset Board</button>
                    </Col>
                    <Col md={10}>
                        {elements}
                        <h3>{this.state.message}</h3>
                    </Col>
                </Row>
            </div>
        );


    };

    updateSquare(squareId) {
        // console.log(this.isGameOver())
        // console.log("updateSquare on cell # " + squareId);
        let newGameBoard = this.state.gameBoard;
        if (this.isGameOver()){
            return;
        }
        let piece = this.state.redTurn ? "R" : "B";
        let pieceToUpdate = (squareId % 7) + 35;
        while (newGameBoard[pieceToUpdate] === "R" || newGameBoard[pieceToUpdate] === "B"){

            pieceToUpdate = pieceToUpdate - 7;
            if (pieceToUpdate < 0){
                let message = "No spaces left. Still " + (this.state.redTurn ? "red's" : "black's") + " turn."
                this.setState({message: message})
                return;
            }

        }

        newGameBoard[pieceToUpdate] = piece;

        let newTurn = !this.state.redTurn;
        let nextTurnMessage = (newTurn ? "Red" : "Black") + "'s turn to make a move."
        this.setState(
            {
                gameBoard: newGameBoard,
                redTurn: newTurn,
                message: nextTurnMessage
            },
        )
        if (this.isGameOver()){
            this.setState({message: (piece === "R" ? "Red": "Black") + " has won the game!"})
        }
        if (!newGameBoard.some(x => x === "-")){
          this.setState({message: "Tie Game"})
        }
    }

    isGameOver() {
        if (this.isHorizontalWin() ||
            this.isVerticalWin() ||
            this.isDiagonalUpRightWin() ||
            this.isDiagonalUpLeftWin()

        ){
            return true;
        }

        return false
    }

    isVerticalWin(){
        let board = this.state.gameBoard;
        for (let row = 0; row < 3; row++){
            for (let col = 0; col < 7; col++){
                let i = row * 7 + col;
                if (board[i] !== "-" && board[i] === board[i+7] && board[i+7] === board[i+14] && board[i+14] === board[i+21]){
                    return true;
                }
            }
        }
    }

    isHorizontalWin(){
        let board = this.state.gameBoard;
        for (let row = 0; row < 6; row++){
            for (let col = 0; col < 4; col++){
                let i = row * 7 + col;
                if (board[i] !== "-" && board[i] === board[i+1] && board[i+1] === board[i+2] && board[i+2] === board[i+3]){
                    return true;
                }
            }
        }
    }

    isDiagonalUpRightWin(){
        let board = this.state.gameBoard;
        for (let row = 0; row < 6; row++){
            for (let col = 0; col < 4; col++){
                let i = row * 7 + col;
                // console.log(i);
                if (i - 18 >= 0) {
                    // console.log(i + " " + board[i] + board[i - 6] + board[i - 12] + board[i - 18])
                    if (board[i] !== "-" && board[i] === board[i -6] && board[i -6] === board[i -12] && board[i -12] === board[i -18]) {
                        return true;
                    }
                }
            }
        }
    }

    isDiagonalUpLeftWin(){
        let board = this.state.gameBoard;
        for (let row = 0; row < 6; row++){
            for (let col = 0; col < 4; col++){
                let i = row * 7 + col;
                // console.log(i);
                if (i - 18 >= 0) {
                    // console.log(i + " " + board[i] + board[i - 6] + board[i - 12] + board[i - 18])
                    if (board[i] !== "-" && board[i] === board[i -8] && board[i -8] === board[i -16] && board[i -16] === board[i -24]) {
                        return true;
                    }
                }
            }
        }
    }

    resetBoardState() {
        this.setState({
            gameBoard: ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-","-", "-", "-", "-", "-", "-", "-"],
            redTurn: true,
            message:"",
        })
    }
}

export default ConnectFour