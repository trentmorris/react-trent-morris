
import React from "react";
import { Row, Col} from "react-bootstrap"

export default function Contact() {

    return (
        <div>
            <Row>
                <Col >
                    <label style={{textIndent: "20px"}}><b>Trent Morris</b></label>
                    <br/>
                    <label style={{textIndent: "30px"}}><b>Email: </b> trent7morris@gmail.com</label>
                    <br/>
                    <label style={{textIndent: "30px"}}><b>LinkedIn: </b> www.linkedin.com/trentmorris</label>
                    <br/>
                    <label style={{textIndent: "30px"}}><b>Github: </b> www.github.com/trentmorris</label>
                </Col>
            </Row>
        </div>

    );
}
