// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import About from './About';
// import * as serviceWorker from './serviceWorker';
// import AppHeader from "./AppHeader";
//
// ReactDOM.render(
//     <React.StrictMode>
//         <AppHeader/>
//         <App/>
//
//     </React.StrictMode>,
//     document.getElementById('root')
// );
//
// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from "./Admin/App";
import Notfound from "./notfound";
import Footer from "./footer";
import AppHeader from "./AppHeader";
import 'bootstrap/dist/css/bootstrap.min.css';
import Contact from "./Admin/Contact";
import TicTacToe from "./TicTacToe/TicTacToe";
import ConnectFour from "./ConnectFour/connectfour";
import Goals from "./Goals/Goals";

const routing = (
    <Router>
        <div>
            <AppHeader />
            <hr />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/TicTacToe" component={TicTacToe}/>
                <Route path="/Contact" component={Contact}/>
                <Route path="/ConnectFour" component={ConnectFour}/>
                <Route path="/Goals" component={Goals}/>
                <Route component={Notfound} />
            </Switch>
            <Footer />
        </div>
    </Router>
);

ReactDOM.render(routing, document.getElementById("root"));