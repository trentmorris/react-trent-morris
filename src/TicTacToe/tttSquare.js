import React from "react";
import "./tttSquare.css"

class TttSquare extends React.Component {
    constructor(props) {
        super(props);
        this.squareId = "square" + props.squareId
        this.handleClick= this.handleClick.bind(this);
    }

    handleClick(){
        this.props.handleClick(this.props.squareId);
    }

    render() {
        return (

            <button id={this.squareId} className="tttSquare" onClick={this.handleClick}>{this.props.squareValue}</button>
        )
    }
}

export default TttSquare