import React from "react";
import TttSquare from "./tttSquare"
import getNextMove from "./computer"
import { Row, Col} from "react-bootstrap"

class TicTacToe extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            gameBoard: ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
            exTurn: true,
            message: "X's turn to make a move.",
            playerMode: "onePlayer",
            firstMove: "human",

        }
        this.updateSquare = this.updateSquare.bind(this);
        this.resetBoardState = this.resetBoardState.bind(this);
        this.updatePlayerMode = this.updatePlayerMode.bind(this);
        this.updateFirstMove = this.updateFirstMove.bind(this);
    }

    render() {
        let elements = []
        for (let i = 0; i < 9; i++) {
            if (i % 3 === 0) {
                elements.push(<br key={"br"+i}/>)
            }
            elements.push(<TttSquare key={"square" + i} squareId={i} squareValue={this.state.gameBoard[i]} handleClick={this.updateSquare}/>)
        }


        return (

            <div>
                <Row>
                    <Col md={{offset:4}} >
                        <h1 style={{textAlign: "left"}}>Tic Tac Toe</h1>
                    </Col>
                </Row>
                <Row>
                    <Col md={{span:2, offset:2}}>
                        <br/>
                        <select className="playerMode" value={this.state.playerMode} onChange={this.updatePlayerMode}>
                            <option value="onePlayer">One Player</option>
                            <option value="twoPlayer">Two Player</option>
                        </select>
                        <div style={{visibility: this.state.playerMode==="onePlayer" ? "visible": "hidden"}}>
                            <br/>
                            <label><b>Who should go first?</b></label>
                            <br/>
                            <input type="radio" value="human" id="human"
                                   onChange={this.updateFirstMove}
                                   checked={this.state.firstMove === "human"}
                                   name="firstMove"/>
                            <label htmlFor="human">&nbsp;Human</label>
                            <br/>

                            <input type="radio" value="computer" id="computer"
                                   onChange={this.updateFirstMove}
                                   checked={this.state.firstMove === "computer"}
                                   name="firstMove"/>
                            <label htmlFor="computer">&nbsp;Computer</label>
                        </div>
                        <br/>
                        <button id="resetButton" className="row-cols-md-3" onClick={this.resetBoardState}>Reset Board</button>
                    </Col>
                    <Col >
                        {elements}
                        <h3>{this.state.message}</h3>
                    </Col>
                </Row>
            </div>
        );


    };

    updateSquare(squareId) {
        // console.log(this.isGameOver())
        // console.log("updateSquare on cell # " + squareId);
        let newGameBoard = this.state.gameBoard;
        if (this.isGameOver() || newGameBoard[squareId] !== "-"){
            return;
        }
        let piece = this.state.exTurn ? "X" : "O";
        newGameBoard[squareId] = piece

        let newTurn = !this.state.exTurn;

        let nextTurnMessage = (newTurn ? "X" : "O") + "'s turn to make a move."
        if (!newGameBoard.some(x => x === "-")){
            nextTurnMessage = "Tie Game";
        }
        if (this.isGameOver()){
            nextTurnMessage = piece + " has won the game!"
        }

        this.setState(
            {
                gameBoard: newGameBoard,
                exTurn: newTurn,
                message: nextTurnMessage
            },
            this.autoMakeMove
        )
    }

    autoMakeMove(){
        if (this.state.playerMode === "onePlayer" && this.state.exTurn === false){

            let squareToUpdate = getNextMove(this.state.gameBoard);
            this.updateSquare(squareToUpdate);
        }
    }

    isGameOver() {

        let board = this.state.gameBoard;
        for (let x of [0, 3, 6]){
            if (board[x] === board[x + 1] && board[x + 1] === board[x + 2] && board[x] !== "-") {
                return true
            }
        }

        for (let x of [0, 1,2]){
            if (board[x] === board[x + 3] && board[x + 3] === board[x + 6] && board[x] !== "-") {
                return true
            }
        }

        if (board[0] === board[4] && board[4] === board[8] && board[4] !== "-") {
            return true
        }

        if (board[2] === board[4] && board[4] === board[6] && board[4] !== "-") {
            return true
        }

        return false
    }

    resetBoardState() {
        this.setState({
            gameBoard: ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
            exTurn: this.state.firstMove === "human",
            message:"X's turn to make a move",
        }, this.autoMakeMove)
    }

    updatePlayerMode(event){
        this.setState( {
            playerMode: event.target.value
        }, this.resetBoardState)
    }

    updateFirstMove(event){
        console.log("update first move " + event.target.value);
        this.setState({
            firstMove: event.target.value
        }, this.resetBoardState)
    }
}

export default TicTacToe
